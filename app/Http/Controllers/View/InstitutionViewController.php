<?php

namespace App\Http\Controllers\View;

use Illuminate\Http\Request;
use App\Permission;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as RQ;
use Carbon\Carbon;  

class InstitutionViewController extends Controller
{
	 public function data()
	 {   

	 	$client = new \GuzzleHttp\Client(['http_errors' => 'false']);
	 	$header = ['Authorization' => 'TerribleTokenZi', 'Content-Type' => 'application/json'];

	 	$request = new RQ('GET', $this->hourcontrol_url.'/api/institution', $header);
	 	$response = $client->send($request, ['timeout' => 10]);
	 	$data['data'] = json_decode($response->getBody(), true);

	 	return $data;
	 }

	 public function index()
	 {   

	 	return view('institution.index');
	 }
}
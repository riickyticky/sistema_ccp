@extends('layouts.app')
@section('content')
<article class="content responsive-tables-page">
                    <div class="title-block">
                        <h1 class="title"> Permiso Tipo Usuario </h1>
                        <p class="title-description"> lista de Permisos </p>
                    </div>
                    <section class="section">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="card-title-block">
                                            <h3 class="title">DataTable Responsivo simple </h3>
                                        </div>
                                        <section class="example">
                                            <div class="table-responsive">
                                                <table id="tableId1" class="table table-striped table-bordered table-hover">
													<thead>
														<tr>
															<th>id</th>
															<th>Nombre Corto</th>
															<th>Nombre Permiso</th>
															<th>activo</th>
														</tr>
													</thead>
												</table>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="section">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="card-title-block">
                                            <h3 class="title">DataTable Responsivo flip-scroll </h3>
                                        </div>
                                        <section class="example">
                                            <div class="table-flip-scroll">
                                            	<table id="tableId2" class="table table-striped table-bordered table-hover flip-content">
													<thead class="flip-header">
														<tr><div class="mobile-menu-handle">
															<th>id</th>
															<th>Nombre Corto</th>
															<th>Nombre Permiso</th>
															<th>activo</th>
														</tr>
													</thead>
												</table>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </article>
@endsection
@section('scriptFooter')
<script>
        $(document).ready(function() {

        	//PARA LA PRIMERA TABLA
            $('#tableId1').DataTable( {
                language: {
                    sProcessing:     "Cargando datos...&nbsp;&nbsp;<img height='32' width='32' src='{{url('dist/img/loader.gif')}}'>",
                    sLengthMenu:     "Mostrar _MENU_ registros",
                    sZeroRecords:    "No se encontraron resultados",
                    sEmptyTable:     "No existe ningún registro en este momento",
                    sInfo:           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    sInfoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    sInfoFiltered:   "(filtrado de un total de _MAX_ registros)",
                    sInfoPostFix:    "",
                    sSearch:         "Buscar:",
                    sUrl:            "",
                    sInfoThousands:  ",",
                    sLoadingRecords: "&nbsp;",
                    oPaginate: {
                        sFirst:    "Primero",
                        sLast:     "Último",
                        sNext:     "Siguiente",
                        sPrevious: "Anterior"
                    },
                    oAria: {
                        sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                        sSortDescending: ": Activar para ordenar la columna de manera descendente"
                    }
                },
                ajax: {
                    url: "{{route('institution.data')}}",
                    type: "GET",
                    beforeSend: function (request) {
                        request.setRequestHeader("X-CSRF-TOKEN", $('input[name="_token"]').val());
                    },
                },
                    //Employee dates: picture, name and postulating button//
                columns: [
                    { data: "id", class: "hidden", defaultContent: "default" },
                    { data: "name", defaultContent: "default" },
                    { data: "shortName", defaultContent: "default"  },
                    { data: "active", defaultContent: "default" },
                ],
                paging: true,
                lengthChange: true,
                searching: true,
                ordering: true,
                info: true,
                autoWidth: true,
                processing: true,
                //order: [[ 12, "desc" ]],
               
            }); 

            //PARA LA SEGUNDA TABLA
            $('#tableId2').DataTable( {
                language: {
                    sProcessing:     "Cargando datos...&nbsp;&nbsp;<img height='32' width='32' src='{{url('dist/img/loader.gif')}}'>",
                    sLengthMenu:     "Mostrar _MENU_ registros",
                    sZeroRecords:    "No se encontraron resultados",
                    sEmptyTable:     "No existe ningún registro en este momento",
                    sInfo:           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    sInfoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    sInfoFiltered:   "(filtrado de un total de _MAX_ registros)",
                    sInfoPostFix:    "",
                    sSearch:         "Buscar:",
                    sUrl:            "",
                    sInfoThousands:  ",",
                    sLoadingRecords: "&nbsp;",
                    oPaginate: {
                        sFirst:    "Primero",
                        sLast:     "Último",
                        sNext:     "Siguiente",
                        sPrevious: "Anterior"
                    },
                    oAria: {
                        sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                        sSortDescending: ": Activar para ordenar la columna de manera descendente"
                    }
                },
                ajax: {
                    url: "{{route('institution.data')}}",
                    type: "GET",
                    beforeSend: function (request) {
                        request.setRequestHeader("X-CSRF-TOKEN", $('input[name="_token"]').val());
                    },
                },
                    //Employee dates: picture, name and postulating button//
                columns: [
                    { data: "id", class: "hidden", defaultContent: "default" },
                    { data: "name", defaultContent: "default" },
                    { data: "shortName", defaultContent: "default"  },
                    { data: "active", defaultContent: "default" },
                ],
                paging: true,
                lengthChange: true,
                searching: true,
                ordering: true,
                info: true,
                autoWidth: true,
                processing: true,
                //order: [[ 12, "desc" ]],
               
            }); 
        });
       
</script>

@endsection
<?php
use Illuminate\Support\Facades\Session;

return [
    'hourcontrol'=> [
        'url' => env('HOURCONTROL_URL', '')
    ]
];
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*REST CONTROLLER*/
Route::prefix('permission')->group(function () {
    Route::get('index', 'Rest\PermissionController@index')->name('permission.index');
});

Route::prefix('user')->group(function () {
    Route::get('index', 'Rest\UserController@index')->name('user.index');
});

Route::prefix('userType')->group(function () {
    Route::get('index', 'Rest\UserTypeController@index')->name('userType.index');
});

Route::prefix('userTypePermission')->group(function () {
    Route::get('index', 'Rest\UserTypePermissionController@index')->name('userTypePermission.index');
});
/*REST CONTROLLER*/

/*VIEW CONTROLLER*/
Route::prefix('institution')->group(function () {
    Route::get('index', 'View\InstitutionViewController@index')->name('institution.index');
    Route::get('data', 'View\InstitutionViewController@data')->name('institution.data');
});
/*VIEW CONTROLLER*/



